
import {
  // add the delete to the imports
  deleteJumbleResolved, 
  // others stay the same
} from './actions'

// add the delete handler
export function* onDeleteJumble(action) {
  // fake some time
  yield call(delay, 5000, true)
  const id = action.payload.id
  // filter out the item
  jumbleList = jumbleList.filter(item=>item.id!==id)
  if(localStorage) localStorage.setItem('jumbles', JSON.stringify(jumbleList));
  yield put(deleteJumbleResolved(id))
  // make sure we go to the list of jumbles
  yield call(history.push, `/jumble`)
}

export default function* () {
  // add a watch for delete action
  yield fork(function* watchDeleteJumble() {
    yield takeEvery(type.deleteJumble, onDeleteJumble)
  })
}
